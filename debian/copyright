Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: OWLAPI
Upstream-Contact: https://github.com/owlcs/owlapi/issues
Source: https://github.com/owlcs/owlapi

Files: *
Copyright: 2003-2022, Ignazio Palmisano <ignazio1977@users.sourceforge.net>
 2011, Clark & Parsia, LLC
 2011, The University of Queensland
 2011, Ulm University
 2011-2014, The University of Manchester
 2014, Commonwealth Scientific and Industrial Research Organisation
 2020, Marc Robin Nolte
License: Apache-2.0 or LGPL-3.0

Files: benchmarks/*
Copyright: 2005, 2014, Oracle and/or its affiliates.
License: GPL-2.0

Files: oboformat/doc/obo-syntax.html
Copyright: 1997-2003, World Wide Web Consortium (MIT, ERCIM, Keio).
License: W3C

Files: tools/src/main/java/uk/ac/manchester/cs/chainsaw/FastSetSimple.java
Copyright: 2011, Ignazio Palmisano, Dmitry Tsarkov, University of Manchester
License: LGPL-2.1+

Files: debian/*
Copyright: 2021-2022, Andrius Merkys <merkys@debian.org>
License: Apache-2.0

License: Apache-2.0
 On Debian systems, the full text of the Apache-2.0 license
 can be found in the file '/usr/share/common-licenses/Apache-2.0'

License: GPL-2.0
 On Debian systems, the full text of the GPL-2.0 license
 can be found in the file '/usr/share/common-licenses/GPL-2'

License: LGPL-2.1+
 On Debian systems, the full text of the LGPL-2.1 license
 can be found in the file '/usr/share/common-licenses/LGPL-2.1'

License: LGPL-3.0
 On Debian systems, the full text of the LGPL-3.0 license
 can be found in the file '/usr/share/common-licenses/LGPL-3'

License: W3C
 This work (and included software, documentation such as READMEs, or
 other related items) is being provided by the copyright holders under
 the following license.
 .
 License
 .
 By obtaining, using and/or copying this work, you (the licensee)
 agree that you have read, understood, and will comply with the
 following terms and conditions.
 .
 Permission to copy, modify, and distribute this software and its
 documentation, with or without modification, for any purpose and
 without fee or royalty is hereby granted, provided that you include
 the following on ALL copies of the software and documentation or
 portions thereof, including modifications:
 .
   * The full text of this NOTICE in a location viewable to users of
     the redistributed or derivative work.
   * Any pre-existing intellectual property disclaimers, notices, or
     terms and conditions. If none exist, the W3C Software Short
     Notice
     (http://www.w3.org/Consortium/Legal/2002/copyright-software-short-notice-20021231.html)
     should be included (hypertext is preferred, text is permitted)
     within the body of any redistributed or derivative code.
   * Notice of any changes or modifications to the files, including
     the date changes were made. (We recommend you provide URIs to the
     location from which the code is derived.)
